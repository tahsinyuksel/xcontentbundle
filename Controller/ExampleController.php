<?php

namespace Ty\XContentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ty\Social\FollowBundle\Entity\TweetMeta;
use Ty\XContentBundle\Model\XBaseContentInterface;

class ExampleController extends Controller
{
    public function indexAction()
    {
        $XContentService = $this->container->get('ty_xcontent.service.tweet_service');

        /** @var XBaseContentInterface $tweet */
        $tweet = $XContentService->createInstance();
        $tweet->setContent("test");
//        $tweet->incrField('commentCount', 1);

        $meta = new TweetMeta();
        $meta->setGroupName('album');
        $meta->setKeyName('cover');
        $meta->setVal('a.jpg');
        $meta->setContent($tweet);

        $tweet->addFeatures($meta);

        $XContentService->save($tweet);

        return $this->render('TyXContentBundle:Default:index.html.twig');
    }
}

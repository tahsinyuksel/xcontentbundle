<?php

namespace Ty\XContentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('TyXContentBundle:Default:index.html.twig');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 08/09/18
 * Time: 17:46
 */

namespace Ty\XContentBundle\Model;


interface XFeaturesInterface
{
    /**
     * @return mixed
     */
    public function getFeatures();

    /**
     * @param mixed $features
     */
    public function setFeatures($features);

    /**
     * @param mixed $feature
     */
    public function addFeatures($feature);
}
<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 08/09/18
 * Time: 17:46
 */

namespace Ty\XContentBundle\Model;


interface XContentSeenInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param mixed $id
     */
    public function setId($id);

    /**
     * @return mixed
     */
    public function getContent();

    /**
     * @param mixed $content
     */
    public function setContent($content);

    /**
     * @return mixed
     */
    public function getFrom();

    /**
     * @param mixed $from
     */
    public function setFrom($from);

    /**
     * @param $status
     * @return mixed
     */
    public function setStatus($status);

    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt);
}
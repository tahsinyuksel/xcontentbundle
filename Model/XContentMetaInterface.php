<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 28/07/18
 * Time: 15:50
 */
namespace Ty\XContentBundle\Model;


/**
 * XContentMetaInterface
 */
interface XContentMetaInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param mixed $id
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getGroupName();

    /**
     * @param string $groupName
     */
    public function setGroupName($groupName);

    /**
     * @return string
     */
    public function getKeyName();

    /**
     * @param string $keyName
     */
    public function setKeyName($keyName);

    /**
     * @return string
     */
    public function getVal();

    /**
     * @param string $val
     */
    public function setVal($val);

    /**
     * @return mixed
     */
    public function getContent();

    /**
     * @param mixed $content
     */
    public function setContent($content);
}
<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 08/09/18
 * Time: 23:05
 */
namespace Ty\XContentBundle\Model\Service;

use Ty\XContentBundle\Model\Dao\BaseDaoImp;

interface XContentServiceImp extends BaseDaoImp
{

    public function getMeta($tweet, $groupNames, $keyNames = array());

    /**
     * @param $tweet
     * @param string $key
     * @param array $val
     * @return array
     */
    public function getMetaByField($tweet, $key, $val);
}
<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 04/11/17
 * Time: 20:43
 */

namespace Ty\XContentBundle\Model\Service;


use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Ty\XContentBundle\Event\BaseDaoEvents;
use Ty\XContentBundle\Model\Dao\BaseDaoImp;

abstract class BaseDaoServiceImp
{
    /** @var  BaseDaoImp */
    protected $daoService;

    /** @var  EventDispatcherInterface */
    protected $dispatcher;

    /** @var string */
    protected $eventName;

    /**
     * constructor.
     * @param BaseDaoImp $daoService
     * @param EventDispatcherInterface $dispatcher
     * @param string $eventName
     */
    public function __construct(
        $daoService,
        EventDispatcherInterface $dispatcher,
        $eventName = ''
    )
    {
        $this->load($daoService, $dispatcher, $eventName);
    }

    /**
     * load
     * @param BaseDaoImp $daoService
     * @param EventDispatcherInterface $dispatcher
     * @param string $eventName
     */
    public function load(
        $daoService,
        EventDispatcherInterface $dispatcher,
        $eventName = ''
    )
    {
        $this->daoService = $daoService;
        $this->dispatcher = $dispatcher;
        $this->eventName = $eventName;
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->daoService->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findAll()
    {
        return $this->daoService->findAll();
    }

    public function find($id)
    {
        return $this->daoService->find($id);
    }

    public function findOneBy(array $criteria)
    {
        return $this->daoService->findOneBy($criteria);
    }

    /**
     * @param mixed $object
     * @return mixed
     */
    public function save($object)
    {
        $id = $object->getId();

        $this->dispatcher->dispatch($this->eventName . BaseDaoEvents::PRE_PERSIST, new GenericEvent($object));

        $this->daoService->save($object);

        $this->dispatcher->dispatch($this->eventName . BaseDaoEvents::POST_PERSIST, new GenericEvent($object));

        if(! $id) {
            $this->dispatcher->dispatch($this->eventName . BaseDaoEvents::SAVE, new GenericEvent($object));
        }

        return $object;
    }

    public function delete($object)
    {
        $this->dispatcher->dispatch($this->eventName . BaseDaoEvents::PRE_DELETE, new GenericEvent($object));

        $result = $this->daoService->delete($object);

        $this->dispatcher->dispatch($this->eventName . BaseDaoEvents::POST_DELETE, new GenericEvent($object));

        return $result;
    }

    public function findByIds($ids)
    {
        return $this->daoService->findByIds($ids);
    }

    public function createInstance()
    {
        return $this->daoService->createInstance();
    }

    public function getRepository()
    {
        return $this->daoService->getRepository();
    }

    public function bulkSave($data, $limit = 0)
    {
        $this->daoService->bulkSave($data, $limit);
    }

    public function bulkDelete($data, $limit = 0)
    {
        $this->daoService->bulkDelete($data, $limit);
    }


}
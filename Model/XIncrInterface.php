<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 08/09/18
 * Time: 17:46
 */

namespace Ty\XContentBundle\Model;


interface XIncrInterface
{
    /**
     * use field val + @param
     *
     * @param string $field
     * @param int $i
     */
    public function incrField($field, $i = 1);
}
<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 23/10/17
 * Time: 17:18
 */

namespace Ty\XContentBundle\Model\Dao;


use Doctrine\Common\Persistence\ObjectManager;

abstract class AbstractObjectDao implements BaseDaoImp
{

    protected $repository;

    protected $om;

    protected $class;

    /**
     * @param ObjectManager $om
     * @param string $class
     */
    public function __construct(ObjectManager $om, $class)
    {
        $this->om = $om;
        $this->load($class);
    }

    public function load($class)
    {
        $this->class = $class;
        $this->repository = $this->om->getRepository($this->getClass());
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     * @param null $limit
     * @param null $offset
     * @return array
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getRepository()->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @param $id
     * @return null|object
     */
    public function find($id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * @param array $criteria
     * @return null|object
     */
    public function findOneBy(array $criteria)
    {
        return $this->getRepository()->findOneBy($criteria);
    }

    public function save($object)
    {
        $this->om->persist($object);
        $this->om->flush();
        return $object;
    }

    public function delete($object)
    {
        $this->om->remove($object);
        $this->om->flush();
    }

    public function objectManager()
    {
        return $this->om;
    }

    public function findByIds($ids)
    {
        return $this->findBy(array('id'=> $ids));
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    public function createInstance()
    {
        return new $this->class();
    }

    public function bulkSave($data, $limit = 0)
    {
        $this->bulkProcess($data, $limit, 'persist');
    }

    public function bulkDelete($data, $limit = 0)
    {
        $this->bulkProcess($data, $limit, 'remove');
    }

    private function bulkProcess($data, $limit = 0, $process = 'persist')
    {
        $i = 1;
        $dataCount = count($data);
        $limit = ($limit == 0) ? $dataCount : $limit;

        foreach($data as $item) {
            $this->om->{$process}($item);

            if($i % $limit === 0) {
                $this->om->flush();
            }

            $i += 1;
        }

        if($dataCount % $limit != 0) {
            $this->om->flush();
        }
    }

    public function getClass()
    {
        if (false !== strpos($this->class, ':')) {
            $metadata = $this->om->getClassMetadata($this->class);
            $this->class = $metadata->getName();
        }
        return $this->class;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 06/11/17
 * Time: 08:50
 */

namespace Ty\XContentBundle\Model\Dao;


interface BaseDaoImp
{
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    public function findAll();

    public function find($id);

    public function findOneBy(array $criteria);

    public function save($object);

    public function delete($object);

    public function findByIds($ids);

    public function createInstance();

    public function getRepository();

    public function bulkSave($data, $limit = 0);

    public function bulkDelete($data, $limit = 0);
}
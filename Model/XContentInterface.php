<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 07/07/18
 * Time: 23:35
 */
namespace Ty\XContentBundle\Model;


interface XContentInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getContent();

    /**
     * @param string $content
     */
    public function setContent($content);

    /**
     * is replay or sub content
     * @return int
     */
    public function getParentId();

    /**
     * @param int $parentId
     */
    public function setParentId($parentId);

    /**
     * target source id
     * @return int
     */
    public function getTargetId();

    /**
     * @param int $targetId
     */
    public function setTargetId($targetId);

    /**
     * @return int
     */
    public function getFromId();

    /**
     * @param int $fromId
     */
    public function setFromId($fromId);

    /**
     * @return int
     */
    public function getLikeCount();

    /**
     * @param int $likeCount
     */
    public function setLikeCount($likeCount);

    /**
     * @return int
     */
    public function getDislikeCount();

    /**
     * @param int $dislikeCount
     */
    public function setDislikeCount($dislikeCount);

    /**
     * @return int
     */
    public function getCommentCount();

    /**
     * @param int $commentCount
     */
    public function setCommentCount($commentCount);

    /**
     * @param $status
     * @return mixed
     */
    public function setStatus($status);

    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @return int
     */
    public function getContentType();

    /**
     * @param int $contentType
     */
    public function setContentType($contentType);

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt);

    /**
     * @return mixed
     */
    public function getUpdatedAt();

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @return mixed
     */
    public function getFeatures();

    /**
     * @param mixed $features
     */
    public function setFeatures($features);

    /**
     * @param mixed $feature
     */
    public function addFeatures($feature);

    /**
     * @param int $i
     */
    public function likeIncr($i = 1);

    /**
     * @param int $i
     */
    public function dislikeIncr($i = 1);

    /**
     * @param int $i
     */
    public function commentIncr($i = 1);

    /**
     * @param string $field
     * @param int $i
     */
    public function incrField($field, $i = 1);
}
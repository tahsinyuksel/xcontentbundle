<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 10/09/18
 * Time: 16:31
 */

namespace Ty\XContentBundle\Event;


final class BaseDaoEvents
{
    /**
     * pre persist
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     */
    const PRE_PERSIST = '.pre_persist';

    /**
     * post persist
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     */
    const POST_PERSIST = '.post_persist';

    /**
     * save
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     */
    const SAVE = '.save';

    /**
     * pre delete
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     */
    const PRE_DELETE = '.pre_delete';

    /**
     * post delete
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     */
    const POST_DELETE = '.post_delete';
}
<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 30/07/18
 * Time: 14:27
 */

namespace Ty\XContentBundle\Service\Util;


class TweetContentUtil
{
    /**
     * @param string $content
     * @return array
     */
    public function detectLinks($content)
    {
        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $content, $match);
        return $match[0];
    }

    /**
     * $2: found target
     * Ex: http://twitter.com/$2
     * Ex: content: test bu @yeni geldi #istanbul 34 @yeni yıl
     *
     * @param string $content
     * @param string $hrefLink
     * @param string $chr
     * @return array
     */
    public function makeMentionLink($content, $hrefLink, $chr = '@')
    {
        $pattern = sprintf('/(^|[^a-z0-9_])%s([a-z0-9_]+)/i', $chr);
        $replace = sprintf('$1<a href="'. $hrefLink .'">%s$2</a>', $chr);
        return preg_replace($pattern, $replace, $content);
    }

    /**
     * Ex: content: test bu @yeni geldi #istanbul 34 @yeni yıl @tr
     * get list mention with selected character
     * @param $content
     * @param string $chr
     * @return array
     */
    public function detectMention($content, $chr = '@')
    {
        $pattern = sprintf('/(^|[^a-z0-9_])%s([a-z0-9_]+)/i', $chr);
        preg_match_all($pattern, $content, $match);
        return $match[0];
    }
}
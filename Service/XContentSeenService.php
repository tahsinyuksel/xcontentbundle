<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 07/07/18
 * Time: 23:41
 */

namespace Ty\XContentBundle\Service;


use Ty\XContentBundle\Model\Service\BaseDaoServiceImp;
use Ty\XContentBundle\Model\XContentSeenInterface;

class XContentSeenService extends BaseDaoServiceImp
{
    public function saveSeen(XContentSeenInterface $seen)
    {
        // validations
        if(!$seen->getFrom() || !$seen->getContent()) {
            throw new \Exception('Seen not valid');
        }

        return $this->save($seen);
    }

    
}
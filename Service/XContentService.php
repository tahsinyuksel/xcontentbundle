<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 07/07/18
 * Time: 23:41
 */

namespace Ty\XContentBundle\Service;


use Ty\XContentBundle\Model\Service\BaseDaoServiceImp;
use Ty\XContentBundle\Model\Service\XContentServiceImp;
use Ty\XContentBundle\Model\XContentMetaInterface;

class XContentService extends BaseDaoServiceImp implements XContentServiceImp
{

    public function getMeta($tweet, $groupNames, $keyNames = array())
    {
        $result = array();

        if(method_exists($tweet, 'getFeatures') && $tweet->getFeatures()) {
            /** @var XContentMetaInterface $feature */
            foreach ($tweet->getFeatures() as $feature) {
                if(in_array($feature->getGroupName(), $groupNames)) {
                    $result[] = $feature;
                }
            }

            if(count($keyNames) > 0) {

                /** @var XContentMetaInterface $feature */
                foreach ($result as $key => $feature) {
                    if(! in_array($feature->getKeyName(), $keyNames)) {
                        unset($result[$key]);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $tweet
     * @param string $key
     * @param array $val
     * @return array
     */
    public function getMetaByField($tweet, $key, $val)
    {
        $result = array();

        if(method_exists($tweet, 'getFeatures') && $tweet->getFeatures()) {

            $method = 'get' . ucfirst($key);

            foreach ($tweet->getFeatures() as $feature) {
                if(in_array($feature->$method(), $val)) {
                    $result[] = $feature;
                }
            }
        }

        return $result;
    }
}
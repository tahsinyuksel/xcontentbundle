# Example Service Define

```sh
    ty_xcontent.service.dao.tweet_dao:
        class: Ty\XContentBundle\Service\Dao\XContentDao
        arguments: ["@doctrine.orm.entity_manager", "Ty\\XContentBundle\\Entity\\Tweet"]

    ty_xcontent.service.tweet_service:
        class: Ty\XContentBundle\Service\XContentService
        arguments:
          - "@ty_xcontent.service.dao.tweet_dao"
          - "@event_dispatcher"
          - "event.tweet.new"

```

# Example Entity


Tweet Entity


```sh
/**
 * Tweet
 *
 * @ORM\Table(name="tweet")
 * @ORM\Entity(repositoryClass="Ty\Social\FollowBundle\Repository\TweetRepository")
 */
class Tweet extends XBaseContent implements XFeaturesInterface
{
    /**
     * @ORM\OneToMany(targetEntity="TweetMeta", mappedBy="tweet", cascade={"persist", "remove"})
     */
    protected $features = array();

    /**
     * Tweet constructor.
     */
    public function __construct()
    {
        $this->features = new ArrayCollection();
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * @param mixed $features
     */
    public function setFeatures($features)
    {
        $this->features = $features;
    }

    /**
     * @param mixed $feature
     */
    public function addFeatures($feature)
    {
        $this->features->add($feature);
        $feature->setContent($this);
    }


}
```


Tweet Meta Entity


```sh
/**
 * TweetMeta
 *
 * @ORM\Table(name="tweet_meta")
 * @ORM\Entity(repositoryClass="Ty\Social\FollowBundle\Repository\TweetMetaRepository")
 */
class TweetMeta extends XBaseContentMeta implements XContentMetaInterface
{
    /**
     * Many Features have One Tweet
     * @ORM\ManyToOne(targetEntity="Tweet", inversedBy="features")
     * @ORM\JoinColumn(name="tweet_id", referencedColumnName="id")
     */
    protected $content;

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

}

```

Controller

```sh
  $XContentService = $this->container->get('ty_xcontent.service.tweet_service');

  /** @var XBaseContentInterface $tweet */
  $tweet = $XContentService->createInstance();
  $tweet->setContent("test");
  $tweet->incrField('commentCount', 1);

  $meta = new TweetMeta();
  $meta->setGroupName('album');
  $meta->setKeyName('cover');
  $meta->setVal('a.jpg');  
  $meta->setTweet($tweet);

  $tweet->addFeatures($meta);

  $XContentService->save($tweet);

```
<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 28/07/18
 * Time: 19:27
 */

namespace Ty\XContentBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Ty\XContentBundle\Model\XContentSeenInterface;

abstract class XBaseContentSeen implements XContentSeenInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="integer", nullable=true)
     */
    protected $content;

    /**
     * @var int
     *
     * @ORM\Column(name="fromId", type="integer", nullable=true)
     */
    protected $from;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    protected $status = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    protected $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @inheritDoc
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @inheritDoc
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @inheritDoc
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @inheritDoc
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @inheritDoc
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @inheritDoc
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

}
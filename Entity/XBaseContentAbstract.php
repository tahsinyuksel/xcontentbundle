<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 28/07/18
 * Time: 19:27
 */

namespace Ty\XContentBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

abstract class XBaseContentAbstract
{

    /**
     * @param int $i
     */
    public function likeIncr($i = 1)
    {
        $this->incrField('likeCount', $i);
    }

    /**
     * @param int $i
     */
    public function dislikeIncr($i = 1)
    {
        $this->incrField('dislikeCount', $i);
    }

    /**
     * @param int $i
     */
    public function commentIncr($i = 1)
    {
        $this->incrField('commentCount', $i);
    }

    /**
     * @param string $field
     * @param int $i
     */
    public function incrField($field, $i = 1)
    {
        $setMethod = 'set'. ucfirst($field);
        $getMethod = 'get'. ucfirst($field);
        if(method_exists($this, $getMethod) && method_exists($this, $setMethod)) {
            $this->$setMethod($this->$getMethod() + $i);
        }
    }
}
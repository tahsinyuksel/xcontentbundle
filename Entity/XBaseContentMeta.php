<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 30/07/18
 * Time: 11:17
 */

namespace Ty\XContentBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Ty\XContentBundle\Model\XContentMetaInterface;

abstract class XBaseContentMeta implements XContentMetaInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="groupName", type="string", length=25, nullable=true)
     */
    protected $groupName;

    /**
     * @var string
     *
     * @ORM\Column(name="keyName", type="string", length=50, nullable=true)
     */
    protected $keyName;

    /**
     * @var string
     *
     * @ORM\Column(name="val", type="string", length=255, nullable=true)
     */
    protected $val;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * @param string $groupName
     */
    public function setGroupName($groupName)
    {
        $this->groupName = $groupName;
    }

    /**
     * @return string
     */
    public function getKeyName()
    {
        return $this->keyName;
    }

    /**
     * @param string $keyName
     */
    public function setKeyName($keyName)
    {
        $this->keyName = $keyName;
    }

    /**
     * @return string
     */
    public function getVal()
    {
        return $this->val;
    }

    /**
     * @param string $val
     */
    public function setVal($val)
    {
        $this->val = $val;
    }

}